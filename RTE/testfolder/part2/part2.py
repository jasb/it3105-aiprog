import rewrite
import part2a

def init(file_name):
	processed_file = rewrite(file_name)
	part2a.run(processed)
	
